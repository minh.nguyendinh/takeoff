# frozen_string_literal: true

require 'spec_helper'
require './lib/step_runner'

describe StepRunner do
  let(:version_output) { "10.196.8.101 gitlab-ee\t9.4.1-ee.0\r\n10.196.2.101 gitlab-ee\t9.4.1-ee.0\r\n10.196.4.101 gitlab-ee\t9.4.1-ee.0\r\n" }
  let(:runner_output) do
    <<~OUTPUT
      git checkout master
      git pull git@gitlab.com:gitlab-org/takeoff.git master
      bundle exec knife download roles/staging-base-deploy-node.json
      Ignoring load balancing check
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap OR roles:staging-base-be-sidekiq-besteffort OR roles:staging-base-be-sidekiq-realtime sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap sudo gitlab-ctl stop mailroom
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap OR roles:staging-base-be-sidekiq-besteffort OR roles:staging-base-be-sidekiq-realtime sudo gitlab-ctl stop sidekiq-cluster
      bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs OR roles:staging-base-fe-web OR roles:staging-base-fe-api OR roles:staging-base-fe-git OR roles:staging-base-be-sidekiq-asap OR roles:staging-base-be-sidekiq-besteffort OR roles:staging-base-be-sidekiq-realtime OR roles:staging-base-fe-registry OR roles:staging-base-deploy-node sudo service chef-client stop
      bundle exec knife download roles/staging-omnibus-version.json
      Ignoring version update to staging
      bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo -E chef-client
      bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo SKIP_POST_DEPLOYMENT_MIGRATIONS=1 gitlab-rake db:migrate
      bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo -E chef-client
      bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo gitlab-ctl restart
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo -E chef-client
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo -E chef-client
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo -E chef-client
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl restart
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap sudo -E chef-client
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap sudo gitlab-ctl restart
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-besteffort sudo -E chef-client
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-besteffort sudo gitlab-ctl restart
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-realtime sudo -E chef-client
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-realtime sudo gitlab-ctl restart
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo -E chef-client
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo gitlab-ctl restart
      bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo gitlab-rake gitlab:track_deployment
      bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs OR roles:staging-base-fe-web OR roles:staging-base-fe-api OR roles:staging-base-fe-git OR roles:staging-base-be-sidekiq-asap OR roles:staging-base-be-sidekiq-besteffort OR roles:staging-base-be-sidekiq-realtime OR roles:staging-base-fe-registry OR roles:staging-base-deploy-node sudo -E service chef-client start
      bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo gitlab-rake db:migrate
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap OR roles:staging-base-be-sidekiq-besteffort OR roles:staging-base-be-sidekiq-realtime OR roles:staging-base-fe-api OR roles:staging-base-fe-git OR roles:staging-base-fe-web sudo gitlab-ctl hup unicorn
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap OR roles:staging-base-be-sidekiq-besteffort OR roles:staging-base-be-sidekiq-realtime sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap OR roles:staging-base-be-sidekiq-besteffort OR roles:staging-base-be-sidekiq-realtime sudo gitlab-ctl restart sidekiq-cluster
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl start
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl deploy-page down
      bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo gitlab-rake db:migrate:status
      New version: 9.4.1-ee.0
    OUTPUT
  end

  before do
    enable_dry_run

    allow_any_instance_of(Steps::VersionCheck).to receive(:dpkg_gitlab_version).and_return(version_output)
  end

  subject { described_class.new('1.0.0', 'staging', 'gitlab/test') }

  it 'outputs the right command' do
    expect(subject).not_to receive(:abort)

    expect { subject.run }.to output(runner_output).to_stdout_from_any_process
  end

  context 'error starting chef' do
    let(:retry_runner_output) do
      <<~OUTPUT
        git checkout master
        git pull git@gitlab.com:gitlab-org/takeoff.git master
        bundle exec knife download roles/staging-base-deploy-node.json
        Ignoring load balancing check
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap OR roles:staging-base-be-sidekiq-besteffort OR roles:staging-base-be-sidekiq-realtime sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap sudo gitlab-ctl stop mailroom
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap OR roles:staging-base-be-sidekiq-besteffort OR roles:staging-base-be-sidekiq-realtime sudo gitlab-ctl stop sidekiq-cluster
        Retrying Steps::ChefStop - Attempt 1 - Error: test error
        bundle exec knife download roles/staging-omnibus-version.json
        Ignoring version update to staging
        bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo SKIP_POST_DEPLOYMENT_MIGRATIONS=1 gitlab-rake db:migrate
        bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-besteffort sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-besteffort sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-realtime sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-realtime sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo gitlab-rake gitlab:track_deployment
        bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs OR roles:staging-base-fe-web OR roles:staging-base-fe-api OR roles:staging-base-fe-git OR roles:staging-base-be-sidekiq-asap OR roles:staging-base-be-sidekiq-besteffort OR roles:staging-base-be-sidekiq-realtime OR roles:staging-base-fe-registry OR roles:staging-base-deploy-node sudo -E service chef-client start
        bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo gitlab-rake db:migrate
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap OR roles:staging-base-be-sidekiq-besteffort OR roles:staging-base-be-sidekiq-realtime OR roles:staging-base-fe-api OR roles:staging-base-fe-git OR roles:staging-base-fe-web sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap OR roles:staging-base-be-sidekiq-besteffort OR roles:staging-base-be-sidekiq-realtime sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap OR roles:staging-base-be-sidekiq-besteffort OR roles:staging-base-be-sidekiq-realtime sudo gitlab-ctl restart sidekiq-cluster
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl start
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl deploy-page down
        bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo gitlab-rake db:migrate:status
        New version: 9.4.1-ee.0
      OUTPUT
    end

    it 'shows the error and retry attempt' do
      allow_any_instance_of(Steps::ChefStop).to receive(:roles_to_stop).and_raise('test error')

      expect_any_instance_of(PostChecks::RetryOnFailure).to receive(:abort).once

      expect { subject.run }.to output(retry_runner_output).to_stdout_from_any_process
    end
  end
end
