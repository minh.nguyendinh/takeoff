# frozen_string_literal: true

require 'rake'

module TaskExampleGroup
  load 'Rakefile'

  def self.included(base)
    base.class_eval do
      let(:task_name) { self.class.top_level_description.sub(/\Arake /, '') }
      let(:tasks) { Rake::Task }
      subject(:task) { tasks[task_name] }

      before do
        Rake::Task.tasks.each(&:reenable)
      end

      Rake::Task.define_task(:environment)
    end
  end
end

RSpec.configure do |config|
  config.define_derived_metadata(file_path: %r{/spec/tasks/}) do |metadata|
    metadata[:type] = :task
  end

  config.include TaskExampleGroup, type: :task
end
