# frozen_string_literal: true

module DeployHelper
  class FakeIoString < String
    def initialize(output = '')
      @output = output
      @ready = output && !output.empty?
    end

    def ready?
      @ready
    end

    def alive
      false
    end

    def <<(o)
      super(o)
    end

    def eof?
      !@ready
    end

    def read_nonblock(_bytes)
      @ready = @output.size < 2

      @output.pop
    end
  end

  def allow_open3_to_return(success, stdout_output = '', stderr_output = '')
    value = double(success?: success)
    thread = double('thread', value: value, alive?: false)

    io = FakeIoString.new(stdout_output)
    err_io = FakeIoString.new(stderr_output)

    allow(Open3).to receive(:popen3).and_yield(nil, io, err_io, thread)
  end

  def enable_dry_run
    Takeoff.configure { |config| config[:dry_run] = true }
  end

  def expect_task_to_complete
    # TODO: Fix this when we move away from `RakeFile`, which has an Object class.
    expect_any_instance_of(Object).not_to receive(:abort)
  end
end

RSpec.configure do |c|
  c.include DeployHelper
end
