# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/post_migrations'

describe Steps::PostMigrations do
  before { enable_dry_run }

  subject { described_class.new(Roles.new('staging'), post_deployment: true) }

  it 'outputs the right command' do
    command = <<~COMMAND
      bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo gitlab-rake db:migrate
    COMMAND

    expect { subject.run }.to output(command).to_stdout_from_any_process
  end
end
