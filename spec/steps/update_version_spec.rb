# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/update_version'

describe Steps::UpdateVersion do
  before { enable_dry_run }

  subject do
    described_class.new(Roles.new('staging'),
                        version: '10.0.0',
                        environment: 'staging',
                        repo: 'gitlab/pre-release')
  end

  it 'outputs the right command' do
    command = <<~COMMAND
      bundle exec knife download roles/staging-omnibus-version.json
      Ignoring version update to staging
    COMMAND

    expect { subject.run }.to output(command).to_stdout_from_any_process
  end
end
