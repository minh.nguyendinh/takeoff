# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/services_restart'

describe Steps::ServicesRestart do
  before { enable_dry_run }

  context 'custom node' do
    subject { described_class.new(Roles.new('staging'), role: 'staging-base-fe-web') }

    it 'outputs the right command' do
      stub_const('Steps::ServicesRestart::CUSTOM_RESTART_NODE', 'staging-base-fe-web')

      command = <<~COMMAND
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart deploy-page
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart gitlab-pages
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart nginx
      COMMAND

      expect { subject.run }.to output(command).to_stdout_from_any_process
    end
  end

  context 'all other nodes' do
    subject { described_class.new(Roles.new('staging'), role: 'gitlab-base-other') }

    it 'outputs the right command' do
      command = <<~COMMAND
        bundle exec knife ssh -e -a ipaddress roles:gitlab-base-other sudo gitlab-ctl restart
      COMMAND

      expect { subject.run }.to output(command).to_stdout_from_any_process
    end
  end
end
