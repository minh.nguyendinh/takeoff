# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/deploy_mediator'

describe Steps::DeployMediator do
  before { enable_dry_run }

  subject { described_class.new(Roles.new('staging')) }

  context 'no custom node' do
    it 'outputs the right command' do
      expect { subject.run && $stdout.flush }.to output(command).to_stdout_from_any_process
    end

    def command
      command = <<~COMMAND
        bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-besteffort sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-besteffort sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-realtime sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-realtime sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo gitlab-ctl restart
      COMMAND
    end
  end

  context 'with custom node' do
    it 'outputs the right command' do
      stub_const('Steps::ServicesRestart::CUSTOM_RESTART_NODE', 'staging-base-fe-web')

      expect { subject.run && $stdout.flush }.to output(command).to_stdout_from_any_process
    end

    def command
      command = <<~COMMAND
        bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart deploy-page
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart gitlab-pages
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-asap sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-besteffort sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-besteffort sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-realtime sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq-realtime sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo -E chef-client
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo gitlab-ctl restart
      COMMAND
    end
  end
end
