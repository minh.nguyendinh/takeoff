# frozen_string_literal: true

require 'spec_helper'
require './lib/command_runner'

describe CommandRunner do
  before { allow_open3_to_return(true) }

  describe '#run_command_on_roles' do
    it 'runs commands with knife' do
      expect(Open3).to receive(:popen3).with("bundle exec knife ssh -e -a ipaddress 'roles:staging-base-fe-api' 'date'")

      described_class.run_command_on_roles('staging-base-fe-api', 'date')
    end
  end
end
