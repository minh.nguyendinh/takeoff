# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class UnicornHup < BaseRoles
    # We need to be super careful with unicorn-web. It must be HUP'ed
    # rather than restarted like all other services.
    ROLE_TO_HUP = 'gitlab-base-fe-web'

    def run
      return unless suitable_roles_to_hup?

      run_command_on_roles roles_to_hup,
                           'sudo gitlab-ctl hup unicorn',
                           title: "Sending HUP to unicorn on #{roles_to_hup}"
    end

    def suitable_roles_to_hup?
      roles_to_hup == ROLE_TO_HUP || roles_to_hup.is_a?(Array)
    end

    private

    def roles_to_hup
      options[:roles_to_hup]
    end
  end
end
