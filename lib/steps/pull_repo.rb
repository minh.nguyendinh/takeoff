# frozen_string_literal: true

require './lib/steps/base'

module Steps
  class PullRepo < Base
    def run
      run_with_progress 'git checkout master', title: 'Switching to master'

      run_with_progress "git pull #{Takeoff.config[:takeoff_repo]} master",
                        title: "Pulling from #{Takeoff.config[:takeoff_repo]}"
    end
  end
end
