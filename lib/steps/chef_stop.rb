# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class ChefStop < BaseRoles
    def run
      run_command_on_roles roles_to_stop,
                           'sudo service chef-client stop',
                           title: "Stopping chef-client on #{roles_to_stop.join(', ')}",
                           silence_stdout: false
    end

    private

    def roles_to_stop
      @roles_to_stop ||= roles.regular_and_blessed
    end
  end
end
