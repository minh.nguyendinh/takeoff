# frozen_string_literal: true

Dir['./lib/steps/*.rb'].each { |file| require file }

module Steps
  def self.[](step_name)
    const_get("Steps::#{step_name.split('_').map(&:capitalize).join}")
  end
end
